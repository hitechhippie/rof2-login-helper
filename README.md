# rof2-login-helper

Fork of Zaela's eqemu-login-helper.. uses port 5999 for RoF2 client. Original: https://github.com/Zaela/eqemu-login-helper

##################
EQEmu Login Helper
##################

Clientside workaround for the lack of out-of-order packet handling in the public EQEmu loginserver.

This is a clone of Zaela's eqemu login helper, modified for RoF2, port 5999 for RoF2 clients

Original (credit to Zaela), here: https://github.com/Zaela/eqemu-login-helper 

*****
Usage
*****

* Change the Host eqhost.txt to localhost:5999
* Start rof2-login-helper and keep it running in the background
* Login as normal